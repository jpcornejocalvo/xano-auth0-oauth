import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { FormService } from '../form.service';
import * as _ from 'lodash';

@Component({
  selector: 'app-form-input',
  templateUrl: './form-input.component.html',
  styleUrls: ['./form-input.component.scss']
})
export class FormInputComponent implements OnInit {
  @Input() type = 'text';
  @Input() class = 'form-control';
  @Input() placeholder = '';
  @Input() form;
  @Input() name;
  @Input() errors = {};

  _values = [];

  @Input() set values(v) {
    this._values = v;
  };

  get values() {
    return this._values;
  }

  @Input() focus = false;
  @Input() disabled:any = undefined;
  @Input() submitted;
  @Input() focus_delay = 500;
  @Output() change = new EventEmitter();

  @ViewChild("inputWrap", { static: true }) inputWrapField: ElementRef;

  static ID = Symbol();
  constructor(
    private formService: FormService
  ) { }

  ngOnInit() {
    if (this.focus) {
      if (["text","textarea"].includes(this.type)) {
        setTimeout(() => {
          if (this.inputWrapField && this.inputWrapField.nativeElement && this.inputWrapField.nativeElement.firstElementChild) {
            this.inputWrapField.nativeElement.firstElementChild.focus();
          }
        }, this.focus_delay);
      }
    }
  }

  getDisabled() {
    return this.disabled ? true : undefined;
  }

  hasErrors() {
    if (typeof this.submitted == "boolean") {
      if (!this.submitted) return false;
    }

    return this.getFormErrors();
  }

  onChange(event) {
    if (typeof this.name == "number") {
      this.change.emit(this.form.controls[this.name].value);
    } else {
      this.change.emit(this.form.get(this.name).value);
    }
  }

  getFormErrors() {
    return this.formService.getFormErrors(this.form, this.name);
  }

  getValue(value) {
    return _.get(value, "value", value);
  }

  getDisplay(value) {
    return _.get(value, "display", this.getValue(value));
  }

  error(type) {
    let ret = this.getFormErrors();
    if (ret[type]) {
      if (Object.keys(ret)[0] == type) {
        return ret[type];
      }
    }

    return false;
  }
}
